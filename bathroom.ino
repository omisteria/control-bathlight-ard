/* Bathroom code

   This code is provided to show how to control the SparkFun
   ProMicro's TX and RX LEDs within a sketch. It also serves
   to explain the difference between Serial.print() and
   Serial1.print().
*/

#define analog8 A8;

int RELE1_PIN = A1;
int RELE2_PIN = A2;
int SWITCH1_PIN = 15; // door
int SWITCH2_PIN = A0; // movement
int ECHO_PIN = 14; 
int TRIG_PIN = A3; 
//int RXLED = 17;  // The RX LED has a defined Arduino pin

boolean valueDoor = false, prevValueDoor = false;
boolean valueMove = false, prevValueMove = false;
boolean valueDst = false;
boolean valueInside = false, prevValueInside = false, valueInsideShifted = false;
boolean valueRele1 = false;
int duration, cm, distanceCnt = 0, distanceSum = 0, distanceCntMax = 3, cntInsideShifted = 0;
unsigned long time1 = 0, time2 = 0, tdiff = 0;

void setup()
{
  pinMode(SWITCH1_PIN, INPUT_PULLUP);
  pinMode(SWITCH2_PIN, INPUT_PULLUP);
  pinMode(RELE1_PIN, OUTPUT);
  pinMode(RELE2_PIN, OUTPUT);
  pinMode(TRIG_PIN, OUTPUT); 
  pinMode(ECHO_PIN, INPUT); 
 //pinMode(RXLED, OUTPUT);  // Set RX LED as an output

 Serial.begin(9600); //This pipes to the serial monitor
 Serial1.begin(9600); //This is the UART, pipes to sensors attached to board
 writeRele(RELE1_PIN, false);
 writeRele(RELE2_PIN, false);
}

void loop()
{
  
  cm = getDistance(cm);
  
  valueDst = (cm < 50 && cm > 9);
  valueDoor = readSensorDoor();
  valueMove = readSensorMove();


/*
Serial.print(cm);
Serial.print(" ");
Serial.print(valueDoor);
Serial.print(" ");
Serial.print(valueMove);
Serial.println();
*/
  

  if (valueDoor) {
    valueInside = false;
  } else if (valueMove || valueDst) {
    valueInside = true;
  }

  if (valueDoor) { // if door is opened
    valueInsideShifted = true;
    cntInsideShifted = 0;    
  } else {
    if (prevValueDoor) cntInsideShifted = 90; // pause ~ 18sec
    if (cntInsideShifted>0) cntInsideShifted--;
    valueInsideShifted = (cntInsideShifted!=0);
  }
  
  prevValueDoor = valueDoor;

  valueRele1 = valueInsideShifted || valueInside;

  writeRele(RELE2_PIN, valueRele1);
  
  delay(200);
}

int getDistance(int oldValue) {
  
  digitalWrite(TRIG_PIN, LOW); 
  delayMicroseconds(2); 
  digitalWrite(TRIG_PIN, HIGH); 
  delayMicroseconds(10); 
  digitalWrite(TRIG_PIN, LOW); 
  duration = pulseIn(ECHO_PIN, HIGH); 
  int res = duration / 58;

  if (res>4 && res<180) {
    distanceCnt++;
    distanceSum = (distanceSum + res) /2;
    if (distanceCnt>distanceCntMax-1) {
      distanceCnt=0;
    }
    
    return distanceSum;
  }
  
  return oldValue;
}
boolean readSensorDoor() {
  return digitalRead(SWITCH1_PIN);
}

int cntDiscr = 0;
boolean readSensorMove() {
  boolean res = digitalRead(SWITCH2_PIN);
   
  if (!prevValueMove && res) {
    cntDiscr = 4; // 0.6sec
  }

  if (cntDiscr>0) cntDiscr--;
  prevValueMove = res;
  
  return (cntDiscr!=0);
}
void writeRele(int pin, boolean value) {
  digitalWrite(pin, value ? LOW : HIGH);
}
